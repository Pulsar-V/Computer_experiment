//序列中第k大元素的查找问题
#include <iostream>
using namespace std;
bool flag=false;
int k;
void quick_sort(int *a,int l,int r) {
    if (l>=r) return;
    int i=l,j=r,x=a[l];
    while (i<j) {
        while (a[j]>=x&&i<j)
            j--;
        if (i<j) a[i++]=a[j];
        while (a[i]<=x&&i<j)
            i++;
        if (i<j) a[j--]=a[i];
    }
    a[i]=x;
    if (i==k-1) {
        cout<<a[i]<<endl;
        flag=true;
        return;
    }
    quick_sort(a, l, i-1);
    quick_sort(a, i+1, r);
}
int main() {
    int a[100],n,k;
    cin>>n>>k;
    for (int i=0; i<n; i++)
        cin>>a[i];
    quick_sort(a,0,n-1);
    if (!flag)
        cout<<a[k-1]<<endl;
    system("pause");
}
