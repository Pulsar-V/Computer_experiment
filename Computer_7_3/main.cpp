//有序序列中元素的lower_bound和upper_bound问题.
#include <iostream>

using namespace std;
int n;

void low_bound(int *a, int x, int y, int v) {
    int m, l = x, r = y;
    while (l < r) {
        m = (l + r) / 2;
        if (a[m] > v) r = m;
        else if (a[m] < v) l = m + 1;
        else r = m;
    }
    if (a[l] == v) {
        cout << v << "的下界是：" << l << endl;
        return;
    }
    n++;
    for (int i = y; i >= l; i--)
        a[i + 1] = a[i];
    a[l] = v;
    cout << v << "的下界是：" << l << endl;
    for (int i = 1; i <= y + 1; i++)
        cout << a[i] << ' ';
    cout << endl;
}

void up_bound(int *a, int x, int y, int v) {
    int m, l = x, r = y;
    while (l < r) {
        m = (l + r) / 2;
        if (a[m] > v) r = m;
        else if (a[m] < v) l = m + 1;
        else l = m + 1;
    }
    if (a[l - 1] == v) {
        cout << v << "的上界是：" << l - 1 << endl;
        return;
    }
    n++;
    for (int i = y; i >= l; i--)
        a[i + 1] = a[i];
    a[l] = v;
    cout << v << "的上界是：" << l << endl;
    for (int i = 1; i <= y + 1; i++)
        cout << a[i] << ' ';
    cout << endl;
}

int main() {
    int a[100], v;
    cin >> n;
    for (int i = 1; i <= n; i++)
        cin >> a[i];
    cin >> v;
    low_bound(a, 1, n, v);
    up_bound(a, 1, n, v);
    system("pause");
}
