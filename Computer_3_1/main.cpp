//3_1利用二叉树进行重言式判别
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define MAX_SIZE 500


typedef struct _tree {
    char data;
    struct _tree *lchild;
    struct _tree *rchild;
    int flag;
} tree;
tree *root;
int var[26];
int turekey[26];
int globeflag;
int typein[] = {3, 5, 7, 1, 8, 0};
int typeout[] = {2, 4, 6, 8, 1, 0};

typedef struct _stack {
    char level[MAX_SIZE];
    tree *node[MAX_SIZE];
    int top;
} stack;

void init(stack *a) {
    a->top = -1;
}

void initTreeWithData(tree **node, char data) {
    *node = (tree *) malloc(sizeof(tree));
    (*node)->lchild = NULL;
    (*node)->rchild = NULL;
    (*node)->data = data;
    (*node)->flag = 0;
}

char getTop(stack *a) {
    return a->level[a->top];
}

int push(stack *a, tree **newnode, char data) {
    if (a->top >= MAX_SIZE - 1) return 0;
    else {
        a->top++;
        a->node[a->top] = *newnode;
        a->level[a->top] = data;
        return 1;
    }
}

int pushChar(stack *a, char data) {
    if (a->top >= MAX_SIZE - 1) return 0;
    else {
        a->top++;
        a->level[a->top] = data;
        return 1;
    }
}

tree **pop(stack *a) {
    tree **node = &(a->node[a->top]);
    a->top--;
    return node;
}

int symChange(char str) {
    switch (str) {
        case '|':
            return 0;
        case '&':
            return 1;
        case '~':
            return 2;
        case '(':
            return 3;
        case ')':
            return 4;
        case '#':
            return 5;
    }
}

int classCompar(char sym1, char sym2) {
    int a, b;
    a = symChange(sym1);
    b = symChange(sym2);
    if (typein[a] > typeout[b]) {
        return 1;
    } else if (typein[a] < typeout[b]) {
        return -1;
    } else return 0;
}

void createTree(tree **parent, tree **child1, tree **child2) {
    (*parent)->lchild = *child1;
    (*parent)->rchild = *child2;
}

int carulate(int a, int b, char c) {
    if (c == '|') {
        if (a == 0 && b == 0) return 0;
        else return 1;
    } else {
        if (a == 1 && b == 1) return 1;
        else return 0;
    }
}

int flase(int a) {
    if (a == 0) return 1;
    else return 0;
}

void treeView(tree *root) {
    if (root->lchild != NULL) {
        treeView(root->lchild);
    } else if (root->data != '~') root->flag = *(turekey + (var[root->data - 'A']) * sizeof(int));
    if (root->rchild != NULL) treeView(root->rchild);
    if (root->lchild != NULL) {
        if (root->data != '0') {
            root->flag = carulate(root->lchild->flag, root->rchild->flag, root->data);
        } else {
            root->flag = flase(root->lchild->flag);
        }
    }
    globeflag = root->flag;
}

void buildTree(char *str) {
    int i = 0, number = 0, com, first, second, sym, flag = 1;
    stack optr, opnd;
    init(&optr);
    pushChar(&optr, '#');
    init(&opnd);
    while (str[i] != '\0') {
        if (str[i] != ' ') {
            tree *newnode;
            initTreeWithData(&newnode, str[i]);
            if (str[i] >= 'A' && str[i] <= 'Z') {
                push(&opnd, &newnode, number);
            } else {
                flag = 1;
                while (flag) {
                    com = classCompar(getTop(&optr), str[i]);
                    if (com == 1) {
                        tree *parent, *child1, *child2;
                        if (getTop(&optr) == '~') {
                            parent = (tree *) malloc(sizeof(tree));
                            parent->data = '0';
                            parent->lchild = NULL;
                            parent->rchild = NULL;
                            child2 = *pop(&optr);
                        } else {
                            parent = *pop(&optr);
                            child2 = *pop(&opnd);
                        }
                        child1 = *pop(&opnd);
                        createTree(&parent, &child1, &child2);
                        push(&opnd, &parent, '0');
                    } else if (com == 0) {
                        pop(&optr);
                        flag = 0;
                    } else {
                        push(&optr, &newnode, str[i]);
                        flag = 0;
                    }
                }
            }
        }
        i++;
    }
    root = *pop(&opnd);
}

int add(int **number, int last, int start) {
    if (*(number + sizeof(int) * start) == 0)
        *(number + sizeof(int) * start) = reinterpret_cast<int *>(1);
    else {
        *(number + sizeof(int) * start) = 0;
        start++;
        add(&(*number), last, start);
    }
    return 0;
}

void choose(int count, int *name) {
    int i;
    for (i = 0; i < count; i++) {
        int key;
        printf("Çë¸ø%c¸³Öµ£¨1»ò0£©:\n", name[i]);
        scanf("%d", &key);
        *(turekey + var[name[i] - 'A'] * sizeof(int)) = key;
    }
}

int main() {
    char str[MAX_SIZE];
    memset(turekey, 0, 26 * sizeof(int));
    memset(var, -1, 26 * sizeof(int));
    int i, j = 0, count = -1, a[100], flag = -1, name[26], status;
    gets(str);
    for (i = 0; str[i] != '\0'; i++) {
        if (str[i] >= 'A' && str[i] <= 'Z' && var[str[i] - 'A'] == -1) {
            count++;
            name[count] = str[i];
            var[str[i] - 'A'] = count;
        }
    }
    str[i] = '#';
    str[i + 1] = '\0';
    buildTree(str);
    for (i = 0; i < pow(2, count + 1); i++) {
        treeView(root);
        if (i == 0) {
            status = globeflag;
        } else if (status != globeflag) {
            flag = 0;
            break;
        }
       // add(&turekey, count, 0);
    }
    if (flag == 0) {
        printf("Satisfatible\n");
        choose(count + 1, name);
        treeView(root);
        printf("result:%d\n", globeflag);
    } else if (globeflag == 1) {
        printf("True forever\n");
    } else {
        printf("False forever\n");
    }
    return 0;
}
