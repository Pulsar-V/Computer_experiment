//图的最短路径实验
#include <iostream>
#include <stack>
using namespace std;

const int inf=0x7fffffff;
stack<int> trace;

struct adjvex{
    int num,w;
    adjvex *next;
};

struct node{
    int dis,last;
    bool flag;
    adjvex *next;
};

void build_G(node *v,adjvex *e,int n,int m) {
    int A,B,C,id=0;
    adjvex *p;
    for (int i=0; i<105; i++)
        v[i].next=NULL;
    for (int i=0; i<m; i++) {
        scanf("%d%d%d",&A,&B,&C);
        e[id].num=B;
        e[id].w=C;
        e[id].next=v[A].next;
        v[A].next=&e[id];
        id++;
        e[id].num=A;
        e[id].w=C;
        e[id].next=v[B].next;
        v[B].next=&e[id];
        id++;
    }
    for (int i=2; i<105; i++) {
        v[i].dis=inf;
        v[i].last=-1;
        v[i].flag=0;
    }
    v[1].dis=0;
    v[1].last=-1;
    v[1].flag=1;
    p=v[1].next;
    while (p!=NULL) {
        v[p->num].dis=p->w;
        v[p->num].last=1;
        p=p->next;
    }
}

void Dijkstra(node *v,int n) {
    int minw,minindex;
    adjvex *p;
    while (1) {
        minw=inf;
        minindex=-1;
        for (int j=2; j<=n; j++) {
            if (v[j].flag==0&&v[j].dis<minw) {
                minw=v[j].dis;
                minindex=j;
            }
        }
        if (minindex==-1) break;
        if (minindex==n) return;
        v[minindex].flag=1;
        p=v[minindex].next;
        while (p!=NULL) {
            if (v[p->num].flag==0&&v[p->num].dis>v[minindex].dis+p->w) {
                v[p->num].dis=v[minindex].dis+p->w;
                v[p->num].last=minindex;
            }
            p=p->next;
        }
    }
}
int main() {
    int N,M,t;
    while (scanf("%d%d",&N,&M)==2) {
        if (N==0&&M==0)
            break;
        node v[105];
        adjvex e[20010];
        build_G(v,e,N,M);
        Dijkstra(v,N);
        printf("%d：",v[N].dis);
        t=N;
        while (t!=1) {
            trace.push(t);
            t=v[t].last;
        }
        printf("1");
        while (!trace.empty()) {
            printf("->%d",trace.top());
            trace.pop();
        }
        printf("\n");
    }
}