cmake_minimum_required(VERSION 3.8)
project(Computer_7_1)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES main.cpp)
add_executable(Computer_7_1 ${SOURCE_FILES})
message(7_1-load-over)
