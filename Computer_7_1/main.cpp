//逆序对问题
#include <iostream>
using namespace std;
int cnt=0;
void merge_sort(int *a,int *t,int l,int r) {
    if (r-l<1) return;
    if (r-l==1) {
        if (a[l]>a[r]) {
            int tem=a[r];
            a[r]=a[l];
            a[l]=tem;
            cnt++;
        }
        return;
    }
    int m=(l+r)/2,p=l,q=m+1,i=l;
    merge_sort(a, t, l, m);
    merge_sort(a, t, m+1, r);
    while (p<=m||q<=r) {
        if ((p<=m&&a[p]<=a[q])||q>r)
            t[i++]=a[p++];
        else {
            t[i++]=a[q++];
            cnt+=m-p+1;
        }
    }
    for (int j=l; j<=r; j++)
        a[j]=t[j];
}
int main() {
    int a[100],t[100],n;
    cin>>n;
    for (int i=0; i<n; i++)
        cin>>a[i];
    merge_sort(a, t, 0, n-1);
    for (int i=0; i<n; i++) {
        cout<<a[i]<<' ';
    }
    cout<<endl<<"逆序数对为"<<cnt<<"对"<<endl;
    system("pause");
}
