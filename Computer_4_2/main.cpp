//4_2倒水问题
#include <stdio.h>
#include <string.h>
#include <c++/cstdlib>

int max, maxa, maxb, maxc, first, last, exist[1000][1000];
long long int queue[1000000];

void bfs(int tcap);

int finish(int a, int b, int c, int tcap);

int main() {
    int n;
    scanf("%d", &n);
    while (n--) {
        memset(exist, 0, sizeof(exist));
        int a, b, c, tcap;
        printf("请输入3个杯子的杯容量和目标容量: ");
        scanf("%d%d%d%d", &a, &b, &c, &tcap);
        maxa = a;
        maxb = b;
        maxc = c;
        max = a;
        exist[0][0] = 1;
        queue[0] = 0;
        if (a == tcap)
            printf("需要 0 步.\n");
        else
            bfs(tcap);
        printf("\n");
    }
    system("pause");
    return 0;
}

void bfs(int tcap) {
    int a, b, c, flag = 0, final, step = 0, order[1000], laststep[100000];
    order[0] = 0;
    laststep[0] = 0;
    first = 0;
    last = 1;
    while (first < last) {
        step = order[first];
        b = queue[first] / 1000;
        c = queue[first] % 1000;
        a = max - b - c;
        if (a != 0) {
            if (b != maxb) {
                if (a + b < maxb && exist[b + a][c] == 0) {
                    exist[b + a][c] = 1;
                    queue[last] = c + 1000 * (b + a);
                    order[last] = step + 1;
                    laststep[last] = first;
                    last++;
                    flag = finish(0, b + a, c, tcap);
                    if (flag == 1) {
                        final = last - 1;
                        break;
                    }
                }
                if (a + b >= maxb && exist[maxb][c] == 0) {
                    exist[maxb][c] = 1;
                    queue[last] = c + 1000 * maxb;
                    order[last] = step + 1;
                    laststep[last] = first;
                    last++;
                    flag = finish(a + b - maxb, maxb, c, tcap);
                    if (flag == 1) {
                        final = last - 1;
                        break;
                    }
                }
            }
            if (c != maxc) {
                if (a + c < maxc && exist[b][a + c] == 0) {
                    exist[b][a + c] = 1;
                    queue[last] = c + a + 1000 * b;
                    order[last] = step + 1;
                    laststep[last] = first;
                    last++;
                    flag = finish(0, b, c + a, tcap);
                    if (flag == 1) {
                        final = last - 1;
                        break;
                    }
                }
                if (a + c >= maxc && exist[b][maxc] == 0) {
                    exist[b][maxc] = 1;
                    queue[last] = maxc + 1000 * b;
                    order[last] = step + 1;
                    laststep[last] = first;
                    last++;
                    flag = finish(a + c - maxc, b, maxc, tcap);
                    if (flag == 1) {
                        final = last - 1;
                        break;
                    }
                }
            }
        }
        if (b != 0) {
            if (a != maxa) {
                if (a + b < maxa && exist[0][c] == 0) {
                    exist[0][c] = 1;
                    queue[last] = c;
                    order[last] = step + 1;
                    laststep[last] = first;
                    last++;
                    flag = finish(a + b, 0, c, tcap);
                    if (flag == 1) {
                        final = last - 1;
                        break;
                    }
                }
                if (a + b >= maxa && exist[b + a - maxa][c] == 0) {
                    exist[b + a - maxa][c] = 1;
                    queue[last] = c + 1000 * (b + a - maxa);
                    order[last] = step + 1;
                    laststep[last] = first;
                    last++;
                    flag = finish(maxa, b + a - maxa, c, tcap);
                    if (flag == 1) {
                        final = last - 1;
                        break;
                    }
                }
            }
            if (c != maxc) {
                if (b + c < maxc && exist[0][b + c] == 0) {
                    exist[0][b + c] = 1;
                    queue[last] = c + b;
                    order[last] = step + 1;
                    laststep[last] = first;
                    last++;
                    flag = finish(a, 0, b + c, tcap);
                    if (flag == 1) {
                        final = last - 1;
                        break;
                    }
                }
                if (b + c >= maxc && exist[b + c - maxc][maxc] == 0) {
                    exist[b + c - maxc][c] = 1;
                    queue[last] = maxc + 1000 * (b + c - maxc);
                    order[last] = step + 1;
                    laststep[last] = first;
                    last++;
                    flag = finish(a, b + c - maxc, maxc, tcap);
                    if (flag == 1) {
                        final = last - 1;
                        break;
                    }
                }
            }
        }
        if (c != 0) {
            if (b != maxb) {
                if (c + b < maxb && exist[b + c][0] == 0) {
                    exist[b + c][0] = 1;
                    queue[last] = 1000 * (b + c);
                    order[last] = step + 1;
                    laststep[last] = first;
                    last++;
                    flag = finish(a, b + a, 0, tcap);
                    if (flag == 1) {
                        final = last - 1;
                        break;
                    }
                }
                if (c + b >= maxb && exist[maxb][c + b - maxb] == 0) {
                    exist[maxb][c + b - maxb] = 1;
                    queue[last] = c + b - maxb + 1000 * maxb;
                    order[last] = step + 1;
                    laststep[last] = first;
                    last++;
                    flag = finish(a, maxb, c + b - maxb, tcap);
                    if (flag == 1) {
                        final = last - 1;
                        break;
                    }
                }
            }
            if (a != maxa) {
                if (a + c < maxa && exist[b][0] == 0) {
                    exist[b][0] = 1;
                    queue[last] = 1000 * b;
                    order[last] = step + 1;
                    laststep[last] = first;
                    last++;
                    flag = finish(a + c, b, 0, tcap);
                    if (flag == 1) {
                        final = last - 1;
                        break;
                    }
                }
                if (a + c >= maxa && exist[b][c + a - maxa] == 0) {
                    exist[b][c + a - maxa] = 1;
                    queue[last] = c + a - maxa + 1000 * b;
                    order[last] = step + 1;
                    laststep[last] = first;
                    last++;
                    flag = finish(maxa, b, c + a - maxa, tcap);
                    if (flag == 1) {
                        final = last - 1;
                        break;
                    }
                }
            }
        }
        first++;
    }
    if (flag == 1) {
        printf("需要 %d 步:\n", order[first + 1]);
        printf("    a b c\n");
        int sort[1000], i;
        for (i = order[first + 1] - 1; i >= 0; i--) {
            sort[i] = final;
            final = laststep[final];
        }
        for (i = 0; i < order[first + 1]; i++) {
            b = queue[sort[i]] / 1000;
            c = queue[sort[i]] % 1000;
            a = max - b - c;
            printf("(%d) %d %d %d\n", i + 1, a, b, c);
        }
    } else
        printf("No.\n");
}

int finish(int a, int b, int c, int tcap) {
    if (a == tcap || b == tcap || c == tcap)
        return 1;
    return 0;
}
