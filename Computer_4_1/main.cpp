//4_1计算机鼓轮设计
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <c++/cstdlib>

int dfsnum, number1[40000], number2[40000], result[100], visit[40000][2];
void create(int n);
void dfs(int number, int n);
void next(int *news, int *s, int flag, int n);
int trans2ten(int *arr, int n);
void trans2two(int *s, int number, int n);

int main() {
    int n;
    memset(visit, 0, sizeof(visit));
    scanf("%d", &n);
    dfsnum = pow(2, n) - 1;
    create(n - 1);
    dfs(0, n - 1);
    system("pause");
    return 0;
}

void create(int n) {
    int i;
    for (i = 0; i < pow(2, n); i++) {
        visit[i][1] = 0;
        visit[i][2] = 0;
        int now[100], next1[100], next2[100];
        memset(now, 0, sizeof(now));
        memset(next1, 0, sizeof(next1));
        memset(next2, 0, sizeof(next2));
        trans2two(now, i, n);
        next(next1, now, 0, n);
        next(next2, now, 1, n);
        number1[i] = trans2ten(next1, n);
        number2[i] = trans2ten(next2, n);
    }
}

void dfs(int number, int n) {
    int s[100];
    memset(s, 0, sizeof(s));
    if (dfsnum >= 0) {
        if (visit[number][1] == 0) {
            dfsnum--;
            visit[number][1] = 1;
            dfs(number1[number], n);
            trans2two(s, number1[number], n);
            printf("%d", s[n - 1]);
            memset(s, 0, sizeof(s));
        }
        if (visit[number][2] == 0) {
            dfsnum--;
            visit[number][2] = 1;
            dfs(number2[number], n);
            trans2two(s, number2[number], n);
            printf("%d", s[n - 1]);
            memset(s, 0, sizeof(s));
        }
    }
}

void next(int *news, int *s, int flag, int n) {
    int i;
    for (i = 0; i < n; i++) {
        if (i != n - 1) news[i] = s[i + 1];
        else news[i] = flag;
    }
}

int trans2ten(int *arr, int n) {
    int i, number = 0;
    for (i = 0; i < n; i++)
        number += arr[i] * pow(2, n - i - 1);
    return number;
}

void trans2two(int *s, int number, int n) {
    while (number) {
        n--;
        s[n] = number % 2;
        number = number / 2;
    }
}
