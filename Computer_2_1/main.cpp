//2_1表达式求值问题
typedef int SElemType;
#include <ctype.h>
#include <c++/cstdlib>
#include "stdio.h"

#define OVERFLOW    -2

typedef int  Status;
#define STACK_INIT_SIZE  100
#define STACKINCREMENT   10

typedef struct{
    SElemType *base;
    SElemType *top;
    int      stacksize;
}SqStack;
Status InitStack(SqStack *S){
    S->base = (SElemType*)malloc(STACK_INIT_SIZE*sizeof(SElemType));

    if(!S->base)
        exit(OVERFLOW);
    S->top=S->base;
    S->stacksize=STACK_INIT_SIZE;
    return 1;
}
Status StackEmpty(SqStack S){
    if(S.top == S.base)
        return 1;
    else
        return 0;
}
Status GetTop(SqStack S, char *e){
    if(S.top == S.base)
        return 0;
    *e = *(S.top-1);
    return 1;
}
Status GetTop(SqStack S, int *e){
    if(S.top == S.base)
        return 0;
    *e = *(S.top-1);
    return 1;
}
Status Push(SqStack *S, char e){
    if((S->top - S->base) >= S->stacksize){
        S->base = (
                SElemType*)realloc(S->base,
                                   (S->stacksize+STACKINCREMENT)*sizeof(SElemType)
                );
        if(!S->base)
            exit(OVERFLOW);
        S->top = S->base +S->stacksize;
        S->stacksize += STACKINCREMENT;
    }
    *(S->top)=e;
    S->top++;
    return 1;
}
Status Pop(SqStack *S, char *e){
    if(S->top == S->base)
        return 0;
    S->top--;
    *e = *(S->top);
    return 1;
}
Status Pop(SqStack *S, int *e){
    if(S->top == S->base)
        return 0;
    S->top--;
    *e = *(S->top);
    return 1;
}
Status ListTraverse(SqStack S,Status (*visit)(SElemType)){
    SElemType *p;
    p=S.base;
    for(p=S.base;p<S.top;p++)
        (*visit)(*p);

    return 1;
}
Status output(SElemType e){
    printf("%d ",e);
    return 1;
}
Status in(char c,char op[]){
    char *p;
    p=op;
    while(*p != '\0'){
        if(c == *p)
            return 1;
        p++;
    }
    return 0;
}
char Precede(char a, char b){
    int i,j;
    char pre[][7]={
            {'>','>','<','<','<','>','>'},
            {'>','>','<','<','<','>','>'},
            {'>','>','>','>','<','>','>'},
            {'>','>','>','>','<','>','>'},
            {'<','<','<','<','<','=','0'},
            {'>','>','>','>','0','>','>'},
            {'<','<','<','<','<','0','='}};
    switch(a){
        case '+': i=0; break;
        case '-': i=1; break;
        case '*': i=2; break;
        case '/': i=3; break;
        case '(': i=4; break;
        case ')': i=5; break;
        case '#': i=6; break;
    }
    switch(b){
        case '+': j=0; break;
        case '-': j=1; break;
        case '*': j=2; break;
        case '/': j=3; break;
        case '(': j=4; break;
        case ')': j=5; break;
        case '#': j=6; break;
    }
    return pre[i][j];
}
int Operate(int a, char theta, int b){
    int i,j,result;
    i=a;
    j=b;

    switch(theta)   {
        case '+': result = i + j; break;
        case '-': result = i - j; break;
        case '*': result = i * j; break;
        case '/': result = i / j; break;
    }
    return result;
}
int getNext(int *n){
    char c;
    *n=0;
    while((c=getchar())==' ');
    if(!isdigit(c)){
        *n=c;
        return 1;
    }
    do    {
        *n=*n*10+(c-'0');
        c=getchar();
    }    while(isdigit(c));
    ungetc(c,stdin);
    return 0;
}

int EvaluateExpression(){

    int n;
    int flag;
    int c;
    char x,theta;
    int a,b;

    char OP[]="+-*/()#";
    SqStack  OPTR;
    SqStack  OPND;

    InitStack(&OPTR);
    Push(&OPTR,'#');
    InitStack(&OPND);
    flag=getNext(&c);

    GetTop(OPTR, &x);
    while(c!='#' || x != '#')
    {
        if(flag == 0)
        {
            Push(&OPND,c);
            flag = getNext(&c);
        }        else
        {
            GetTop(OPTR, &x);
            switch(Precede(x, c))
            {
                case '<':
                    Push(&OPTR,c);
                    flag = getNext(&c);
                    break;
                case '=':
                    Pop(&OPTR,&x);
                    flag = getNext(&c);
                    break;
                case '>':
                    Pop(&OPTR, &theta);
                    Pop(&OPND,&b);
                    Pop(&OPND,&a);
                    Push(&OPND, Operate(a, theta, b));
                    break;
            }
        }
        GetTop(OPTR, &x);
    }
    GetTop(OPND, &c);
    return c;
}

int main(){
    int c,t;
    scanf("%d",&t);
    while(t--) {
        printf("输入表达式:");
        //表达式检错
        c = EvaluateExpression();
        printf("%d\n", c);
    }
    return 0;
}