//5图的最小生成树实验
#include <iostream>
using namespace std;

int V,E;
class edge {
public:
    int weight,from,to;
    bool flag=false;
};

int cmp(const void *p1,const void *p2) {
    return ((class edge*)p1)->weight-((class edge*)p2)->weight;
}

int union_find(int *v,int x) {
    return (v[x]==x)?x:(v[x]=union_find(v, v[x]));
}

void MST(edge *e,int *v) {
    int vnum=0,all_weight=0,x,y;
    for (int i=0; i<E&&vnum<V-1; i++) {
        x=union_find(v, e[i].from);
        y=union_find(v, e[i].to);
        if (x!=y) {
            v[x]=y;
            e[i].flag=true;
            all_weight+=e[i].weight;
            vnum++;
        }
    }
    cout<<all_weight<<endl;
}

int main() {
    char c1,c2;
    cin>>V>>E;
    edge *e=new edge[E];
    int *v=new int[V];
    for (int i=0; i<V; i++)
        v[i]=i;
    for (int i=0; i<E; i++) {
        cin>>c1>>c2>>e[i].weight;
        e[i].from=c1-'a';
        e[i].to=c2-'a';
    }
    qsort(e, E, sizeof(edge), cmp);
    MST(e, v);
    delete [] e;
    delete [] v;
}

