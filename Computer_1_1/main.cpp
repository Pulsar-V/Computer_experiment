//两个整系数多项式乘法运算问题
#include <stdio.h>
#include <string.h>

#define BUFFER_SIZE 1000

typedef struct _list {
    int element[BUFFER_SIZE];
    int elementsize;
    int listsize;
} list;

void initlist(list *alist) {
    alist->listsize = BUFFER_SIZE;
    memset(alist->element, 0, BUFFER_SIZE * sizeof(int));
}

void print(int coe, int power, int first) {
    if (first) {
        if (power == 0) printf("%d", coe);
        else if (power == 1) printf("%d*x", coe);
        else if (coe == 1) printf("x^%d", power);
        else if (coe == 0);
        else printf("%d*x^%d", coe, power);
    } else {
        if (coe > 0) {
            if (power == 0) printf(" + %d", coe);
            else if (power == 1) printf(" + %d*x", coe);
            else printf(" + %d*x^%d", coe, power);
        } else if (coe < 0) {
            if (power == 0) printf(" - %d", -coe);
            else if (power == 1) printf(" - %d*x", -coe);
            else if (coe == -1) printf(" - x^%d", power);
            else printf(" - %d*x^%d", -coe, power);
        }
    }
}

int main() {
    int times;
    scanf("%d", &times);
    while (times--) {
        int i, j, x, k = 0, n, m, maxn;
        list ploy1, ploy2, ploy3;
        initlist(&ploy1);
        initlist(&ploy2);
        initlist(&ploy3);
        scanf("%d", &n);
        for (i = n; i >= 0; i--) {
            scanf("%d", &ploy1.element[i]);
        }
        scanf("%d", &m);
        for (i = m; i >= 0; i--) {
            scanf("%d", &ploy2.element[i]);
        }
        maxn = m + n;
        for (i = n; i >= 0; i--) {
            x = maxn - k;
            for (j = m; j >= 0; j--) {
                ploy3.element[x] = ploy3.element[x] + ploy1.element[i] * ploy2.element[j];
                x--;
            }
            k++;
        }
        k = 0;
        for (i = maxn; i >= 0; i--) {
            if (ploy3.element[i] != 0) {
                print(ploy3.element[i], i, k == 0);
                k++;
            }
        }
        printf("\n");
    }
    return 0;
}
