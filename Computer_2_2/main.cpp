//2_2����ƥ��ļ���
#include <stdio.h>
#include <stdlib.h>

#define STACKINCREAMENT 10
#define STACK_INIT_SIZE 100
#define OVERFLOW -2
#define ERROR 0
typedef int status;
typedef char SELEMTYPE;
typedef struct {
    SELEMTYPE *base;
    SELEMTYPE *top;
    status stacksize;
} sqstack;

status Init(sqstack *s) {
    s->base = (SELEMTYPE *) malloc(STACK_INIT_SIZE * sizeof(SELEMTYPE));
    if (!s->base)
        exit(OVERFLOW);
    s->top = s->base;
    s->stacksize = STACK_INIT_SIZE;
    return 1;
}

status Gettop(sqstack *s, SELEMTYPE e) {
    if (s->top == s->base)
        return ERROR;
    e = *(s->top - 1);
    return 1;
}

status push(sqstack *s, SELEMTYPE e) {
    if (s->top - s->base >= s->stacksize) {
        s->base = (SELEMTYPE *) realloc(s->base, (s->stacksize + STACKINCREAMENT) * sizeof(SELEMTYPE));
        if (!s->base)
            exit(OVERFLOW);
        s->top = s->base + s->stacksize;
        s->stacksize += STACKINCREAMENT;
    }
    *s->top++ = e;
    return 1;
}

status pop(sqstack *s, SELEMTYPE *e) {
    if (s->top == s->base)
        return ERROR;
    *e = *--s->top;
    return 1;
}

status stackempty(sqstack *s) {
    if (s->top == s->base)
        return 1;
    return ERROR;
}

status clearstack(sqstack *s) {
    if (s->top == s->base)
        return ERROR;
    s->top = s->base;
    return 1;
}

status Parenthesis_match(sqstack *s, char *str) {
    int i = 0, flag = 0;
    SELEMTYPE e;
    while (str[i] != '\0') {
        switch (str[i]) {
            case '(':
                push(s, str[i]);
                break;
            case '[':
                push(s, str[i]);
                break;
            case ')': {
                pop(s, &e);
                if (e != '(')
                    flag = 1;
            }
                break;
            case ']': {
                pop(s, &e);
                if (e != '[')
                    flag = 1;
            }
                break;
            default:
                break;
        }
        if (flag)
            break;
        i++;
    }
    if (!flag && stackempty(s))
        printf("����ƥ��ɹ�!\n");
    else
        printf("����ƥ��ʧ��!\n");
    return 1;
}

int main() {
    char str[100], enter;
    sqstack s;
    Init(&s);
    printf("�������ַ���:\n");
    scanf("%s", str);
    scanf("%c", &enter);
    Parenthesis_match(&s, str);
    system("pause");
    return 0;
}  