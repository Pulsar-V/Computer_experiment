//3_2利用最优二叉树进行文本文件的编码与解码操作
#include <stdio.h>
#include <string.h>
#include <c++/cstdlib>

#define MAXLEN 256
typedef struct HuffCode {
    int code[MAXLEN];
    int length;
} HuffCode;

typedef struct HTNode {
    int weight;
    char name;
    int parent, lchild, rchild;
} HTNode;

HTNode *ht;

int CountFreq(FILE *fp, int *freq, int *name);

void HuffmanCoding(int *freq, HuffCode *hCode, int n, int *name);

void FileCoding(FILE *SFile, FILE *DFile, HuffCode *hCode, int *freq, int bytesCount);

void solve(HTNode *node, int typeCount) {
    char str[200], dfile[200], read;
    int i, flop;
    HTNode root, now;
    printf("请输入解码文件：\n");
    scanf("%s", dfile);
    printf("请输入解码后生成的文件名：\n");
    scanf("%s", str);
    FILE *sfp = (FILE *) fopen(str, "wb");
    FILE *dfp = (FILE *) fopen(dfile, "rb");
    for (i = 0; i < typeCount; i++) {
        if (node[i].parent == -1) {
            root = node[i];
            break;
        }
    }
    now = root;
    while ((read = fgetc(dfp)) != EOF) {
        flop = 1;
        while (flop) {
            if (now.lchild == -1) {
                fputc(now.name, sfp);
                flop = 0;
            } else {
                if (read == '0') {
                    now = node[now.lchild];
                } else {
                    now = node[now.rchild];
                }
            }
        }
    }
    fclose(sfp);
    fclose(dfp);
}

int main() {
    char sFile[200], dFile[200];
    scanf("%s%s", sFile, dFile);
    FILE *sfp = (FILE *) fopen(sFile, "rb");
    if (sfp == NULL) {
        printf("源文件%s打不开!\n", sFile);
        exit(0);
    }
    FILE *dfp = (FILE *) fopen(dFile, "wb");
    if (dfp == NULL) {
        printf("目标文件%s打不开!\n", dFile);
        fclose(sfp);
        exit(0);
    }
    int i, typeCount, freq[MAXLEN], tFreq[MAXLEN], name[MAXLEN];
    int types[MAXLEN];
    int bytesCount = CountFreq(sfp, freq, name);
    for (i = 0, typeCount = 0; i < MAXLEN; i++) {
        if (freq[i] != 0) {
            tFreq[typeCount] = freq[i];
            types[typeCount] = i;
            typeCount++;
        }
    }
    if (typeCount == 0) {
        printf("文件为空文件!\n");
        exit(0);
    }
    HuffCode hCode[MAXLEN];
    HuffmanCoding(tFreq, hCode, typeCount, name);
    for (i = typeCount - 1; i >= 0; i--) {
        hCode[types[i]] = hCode[i];
        memset(&hCode[i], -1, sizeof(HuffCode));
    }
    FileCoding(sfp, dfp, hCode, freq, bytesCount);
    fclose(sfp);
    fclose(dfp);
    printf("文件编码完成!\n");
    solve(ht, typeCount);
    printf("文件解码完成\n");
    return 0;
}

int CountFreq(FILE *fp, int *freq, int *name) {
    int ch, cnt = 0;
    memset(freq, 0, MAXLEN * sizeof(int));
    while ((ch = fgetc(fp)) != EOF) {
        freq[ch]++;
        name[freq[ch]] = ch;
        cnt++;
    }
    return cnt;
}

void HuffmanCoding(int *freq, HuffCode *hCode, int n, int *name) {
    if (n == 1) {
        hCode[0].code[0] = 0;
        hCode[0].length = 1;
        return;
    }
    ht = (HTNode *) malloc((2 * n - 1) * sizeof(HTNode));
    memset(ht, -1, (2 * n - 1) * sizeof(HTNode));
    int i, j, s, t;
    for (i = 0; i < n; i++) {
        ht[i].weight = freq[i];
        ht[i].name = name[freq[i]];
    }
    for (i = n; i < 2 * n - 1; i++) {
        for (j = 0; j < i; j++) {
            if (ht[j].parent == -1) {
                s = j;
                break;
            }
        }
        for (j++; j < i; j++) {
            if (ht[j].parent == -1) {
                if (ht[s].weight > ht[j].weight) {
                    t = s;
                    s = j;
                } else
                    t = j;
                break;
            }
        }
        for (j++; j < i; j++) {
            if (ht[j].parent == -1) {
                if (ht[j].weight < ht[s].weight) {
                    t = s;
                    s = j;
                } else if (ht[j].weight < ht[t].weight) {
                    t = j;
                }
            }
        }
        ht[s].parent = ht[t].parent = i;
        ht[i].lchild = s;
        ht[i].rchild = t;
        ht[i].weight = ht[s].weight + ht[t].weight;
    }
    int node;
    int a[MAXLEN];
    for (i = 0; i < n; i++) {
        for (j = 0, node = i; ht[node].parent >= 0; j++) {
            if (ht[ht[node].parent].lchild == node)
                a[j] = 0;
            else
                a[j] = 1;
            node = ht[node].parent;
        }
        s = 0;
        hCode[i].length = j;
        while (j--)
            hCode[i].code[s++] = a[j];
    }
}

void FileCoding(FILE *SFile, FILE *DFile, HuffCode *hCode, int *freq, int bytesCount) {
    rewind(SFile);
    int i, a[8] = {1, 2, 4, 8, 16, 32, 64, 128};
    int byte = 0, ch, ibyte = 0;
    while ((ch = fgetc(SFile)) != EOF) {
        for (i = 0; i < hCode[ch].length; i++) {
            if (hCode[ch].code[i] == 1)
                byte |= a[ibyte];
            ibyte++;
            if (ibyte == 8) {
                fputc(byte, DFile);
                byte = 0;
                ibyte = 0;
            }
        }
    }
    if (ibyte > 0)
        fputc(byte, DFile);
}
