#include <stdio.h>
#include <stdlib.h>

typedef struct _person {
    int id;
    int number;
    struct _person *next;
} person;

int main() {
    int times;
    scanf("%d", &times);
    while (times--) {
        int i, m, n, maxn, counter;
        person *pre, *last, *newnode;
        scanf("%d %d", &n, &m);
        last = (person *) malloc(sizeof(person));
        last->number = 1;
        scanf("%d", &last->id);
        pre = last;
        for (i = 2; i <= n; i++) {
            newnode = (person *) malloc(sizeof(person));
            newnode->number = i;
            scanf("%d", &newnode->id);
            pre->next = newnode;
            pre = newnode;
        }
        pre->next = last;
        for (i = 0; i < n - 1; i++) {
            if (i == 0) {
                maxn = m;
            }
            counter = 1;
            while (counter < maxn) {
                pre = last;
                last = last->next;
                counter++;
            }
            pre->next = last->next;
            maxn = last->id;
            printf("%d ", last->number);
            free(last);
            last = pre->next;
        }
        printf("%d\n", last->number);
        free(last);
    }
    system("pause");
    return 0;
}
